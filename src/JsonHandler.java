import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import mediaWiki.Page;

public class JsonHandler {

	private static String fileName = "saved_articles.json";
	
	public static void addPageToJson(Page page){
		
		File file = new File(fileName);
		Gson gson = new GsonBuilder()
				  .setPrettyPrinting()
				  .create();
		
		if(!file.exists() || file.length() == 0) {
			List<Page> pages = new ArrayList<>();
			pages.add(page);
			
			try(Writer writer = new FileWriter(file)) {
				gson.toJson(pages, writer);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {

			List<Page> pages = readPagesFromJosn();
			pages.add(page);
			
			try(Writer writer = new FileWriter(file)) {
				gson.toJson(pages, writer);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static List<Page> readPagesFromJosn(){

		File file = new File(fileName);
		if(!file.exists() || file.length() == 0) return null;
		
		Gson gson = new GsonBuilder()
				  .setPrettyPrinting()
				  .create();
		try(Reader reader = new FileReader(file)){
			List<Page> pages = gson.fromJson(reader, new TypeToken<List<Page>>(){}.getType());
			return pages;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void removePageFromJson(Page lastPage) {
		File file = new File(fileName);
		if(!file.exists() || file.length() == 0) return;
		Gson gson = new GsonBuilder()
				  .setPrettyPrinting()
				  .create();
		
		List<Page> pages = readPagesFromJosn();
		List<Page> pagesToRemove = new ArrayList<>();
		for (Page page : pages) {
			if(page.getPageid().equals(lastPage.getPageid())) pagesToRemove.add(page);
		}
		pages.removeAll(pagesToRemove);
		
		try(Writer writer = new FileWriter(file)) {
			gson.toJson(pages, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
