import mediaWiki.WikiAPI;
import mediaWiki.WikiServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.Map;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import mediaWiki.CategoryMember;
import mediaWiki.Page;

public class Main extends Application{
	
	private ListView<Label> articles;
    private WebEngine engine;
    private Page lastPage;
	
	public static void main(String[] args) {
		
		Application.launch(args);
		
	}
	


	@Override
	public void start(Stage stage) throws Exception {
		

	    WebView  browser = new WebView();
	    engine = browser.getEngine();
	    

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        
        Button load = new Button("Load Articles");
        load.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				List<Page> pages = JsonHandler.readPagesFromJosn();
				articles.getItems().clear();
				if(pages != null) {
					for (Page page : pages) {
						Label label = new Label(page.getTitle());
						label.setOnMouseClicked(new EventHandler<Event>() {
							public void handle(Event event) {
								engine.load(page.getFullurl());
								lastPage = page;
							}
						});
						articles.getItems().add(label);
					}
				}
			}
		});
        
        Button remove = new Button("Remove Article");
        remove.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if(lastPage == null) return;
				JsonHandler.removePageFromJson(lastPage);
				load.fire();
			}
		});
        
      
		
		
		Button search = new Button("Randomize");
		search.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				setPages();
			}
		});
		
		Button save = new Button("Save Article");
		save.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if(lastPage == null) return;
				JsonHandler.addPageToJson(lastPage);
			}
		});
		
		
		ToolBar toolBar = new ToolBar();
		toolBar.getItems().addAll(search, load, spacer, remove, save);
		toolBar.centerShapeProperty();
		articles = new ListView<Label>();
		ScrollPane scrollpane = new ScrollPane(articles);
		scrollpane.setFitToHeight(true);
		
		
		BorderPane border = new BorderPane();
		border.setTop(toolBar);
		border.setLeft(scrollpane);
		border.setCenter(browser);
    	Scene scene = new Scene(border,1200,800, Color.web("#666970"));
    	stage.setScene(scene);
    	stage.setTitle("Random Wiki Article Generator");
    	stage.show();
		
	}
	
	private void setPages() {

		WikiAPI wikiApi = WikiServices.getInstance();
		Call<CategoryMember> call = wikiApi.getRandomArticle("query", "random", 0, "json", "info", "url", 30);
		call.enqueue(new Callback<CategoryMember>() {
			
			@Override
			public void onResponse(Call<CategoryMember> call, Response<CategoryMember> response) {
				if(!response.isSuccessful()){
					System.out.println(response.code());
					System.out.println(response.message());
					return;
				}
				Platform.runLater(new Runnable() {
					
					@Override
					public void run() {
						articles.getItems().clear();
						Map<Integer, Page> pages = response.body().getQuery().getPages();
						for (Page page : pages.values()) {
							Label label = new Label(page.getTitle());
							label.setOnMouseClicked(new EventHandler<Event>() {
								public void handle(Event event) {
									engine.load(page.getFullurl());
									lastPage = page;
								}
							});
							articles.getItems().add(label);
						}
						
					}
				});
			}
			
			@Override
			public void onFailure(Call<CategoryMember> call, Throwable t) {
				System.out.println(t.getMessage());
			}
		});
	}
}
