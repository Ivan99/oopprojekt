package mediaWiki;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.Call;

public interface WikiAPI {
	
	@GET(".")
	Call<CategoryMember> getRandomArticle(
			@Query("action") String action, 
			@Query("generator") String generator,
			@Query("grnnamespace") Integer grnnamespace,
			@Query("format") String format,
			@Query("prop") String prop,
			@Query("inprop") String inprop,
			@Query("grnlimit") Integer grnlimit);
}
