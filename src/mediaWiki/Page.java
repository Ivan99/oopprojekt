package mediaWiki;

public class Page {
	
	private Integer pageid;
	private String title;
	private String fullurl;
	
	public Page() {
		super();
	}

	public Integer getPageid() {
		return pageid;
	}

	public String getTitle() {
		return title;
	}

	public String getFullurl() {
		return fullurl;
	}
	
	@Override
	public String toString() {
		return String.format("%s, %s, %s", pageid, title, fullurl);
	}
}
