package mediaWiki;

import java.util.Map;

public class MemberQuery {

	private Map<Integer, Page> pages;
	
	public MemberQuery() {
		super();
	}

	public Map<Integer, Page> getPages() {
		return pages;
	}
}
