package mediaWiki;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WikiServices {
	private static final String BASE_URL = "https://en.wikipedia.org/w/api.php/";

	private static WikiAPI instance;

    public static WikiAPI getInstance() {
        if (instance == null) {
        	Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
            instance = retrofit.create(WikiAPI.class);
        }
        return instance;
    }
}
